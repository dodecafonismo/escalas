module Main exposing (main)

import Browser
import State exposing (init, subscriptions, update)
import Types exposing (..)
import View exposing (view)


main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
