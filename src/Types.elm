module Types exposing (Escala, Model, Msg(..), Score)
import File exposing (File)

type Msg
    = SelectScores
    | GotFiles File (List File)
    | DragEnter
    | DragLeave
    | Añadir String String
    | Descargar
    | Introducir String
    | CambiarNombreEscala String


type alias Score =
    { contents : String
    , filename : String
    }


type alias Escala =
    { nombre : String
    , lista : List Int
    }


type alias Model =
    { hover : Bool
    , scores : List Score
    , escala : Escala
    }
