module State exposing (init, subscriptions, update)

import File.Select as Select
import File.Download as Download
import File exposing (File)
import Types exposing (..)
import Functions exposing (modify)
import Task


init : () -> (Model, Cmd Msg)
init _ =
  ( { hover = False
    , scores = []
    , escala =
        { nombre = "Cromática"
        , lista = [0,1,2,3,4,5,6,7,8,9,10,11]
        }
    }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SelectScores ->
            ( model
            , Select.files ["application/octet-stream"] GotFiles
            )

        GotFiles file files ->
          let newFiles =
                file :: files
              oneCmd f =
                Task.perform (Añadir (File.name f)) (File.toString f)
          in
          ( { model
            | hover = False
            , scores = []
            }
            , Cmd.batch (List.map oneCmd newFiles)
          )

        Añadir name str ->
          let
              newScore =
                  { contents = str
                  , filename = name
                  }
          in
          ( { model
            | scores = newScore :: model.scores
            , hover = False
            }
          , Cmd.none
          )

        DragEnter ->
          ( { model | hover = True }
          , Cmd.none
          )

        DragLeave ->
          ( { model | hover = False }
          , Cmd.none
          )

        Descargar ->
          let newScores =
                List.map (modify model.escala) model.scores
              oneCmd score =
                (Download.string score.filename "text/plain" score.contents)
          in
          ( model
          , Cmd.batch (List.map oneCmd newScores)
          )

        Introducir str ->
            let parsed =
                  parse str
                oldEscala =
                    model.escala
                newEscala =
                    if List.length parsed >= 12 then
                      oldEscala

                    else
                      { oldEscala | lista = parsed }
            in
            ( { model | escala = newEscala }
            , Cmd.none
            )

        CambiarNombreEscala s ->
            let oldEscala =
                    model.escala
                newEscala =
                    { oldEscala | nombre = s }
            in
            ( { model | escala = newEscala }
            , Cmd.none
            )

parse : String -> List Int
parse str =
  str
  |> String.words
  |> List.map (indivParse << String.toLower)

indivParse : String -> Int
indivParse s =
  case s of
    "0" -> 0
    "do" -> 0
    "c" -> 0
    "si#" -> 0
    "b#" -> 0
    --
    "1" -> 1
    "do#" -> 1
    "c#" -> 1
    "reb" -> 1
    "db" -> 1
    --
    "2" -> 2
    "re" -> 2
    "d" -> 2
    --
    "3" -> 3
    "re#" -> 3
    "d#" -> 3
    "mib" -> 3
    "eb" -> 3
    --
    "4" -> 4
    "mi" -> 4
    "e" -> 4
    "fb" -> 4
    "fab" -> 4
    --
    "5" -> 5
    "fa" -> 5
    "f" -> 5
    "mi#" -> 5
    "e#" -> 5
    --
    "6" -> 6
    "fa#" -> 6
    "f#" -> 6
    "solb" -> 6
    "gb" -> 6
    --
    "7" -> 7
    "sol" -> 7
    "g" -> 7
    --
    "8" -> 8
    "sol#" -> 8
    "g#" -> 8
    "lab" -> 8
    "ab" -> 8
    --
    "9" -> 9
    "la" -> 9
    "a" -> 9
    --
    "10" -> 10
    "la#" -> 10
    "a#" -> 10
    "sib" -> 10
    "bb" -> 10
    --
    "11" -> 11
    "si" -> 11
    "b" -> 11
    "dob" -> 11
    "cb" -> 11
    --
    _ -> 0


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
