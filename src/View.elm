module View exposing (view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Info exposing (informacion)
import Json.Decode as D
import Styles exposing (..)
import Types exposing (..)
import Credits exposing (..)
import File exposing (File)

dragNdrop : Model -> Html Msg
dragNdrop model =
  div
    ( dropStyle model.hover ++
      [ hijackOn "dragenter" (D.succeed DragEnter)
      , hijackOn "dragover" (D.succeed DragEnter)
      , hijackOn "dragleave" (D.succeed DragLeave)
      , hijackOn "drop" dropDecoder
      ]
    )
    ([ button
        ( buttonStyle "150px" ++
          [ onClick SelectScores
          , style "margin" "20px" ]
        )
        [ text "Examinar" ]
    ] ++
      ( List.map
        (\score ->
          span
            ([ style "color" "#ccc" ] ++ textStyle "1em")
            [ text (.filename score) ]
        )
        model.scores
      )
    )

dropDecoder : D.Decoder Msg
dropDecoder =
  D.at ["dataTransfer","files"] (D.oneOrMore GotFiles File.decoder)

hijackOn : String -> D.Decoder msg -> Attribute msg
hijackOn event decoder =
  preventDefaultOn event (D.map hijack decoder)

hijack : msg -> (msg, Bool)
hijack msg =
  (msg, True)


nombreEscala : Html Msg
nombreEscala =
  div
  []
  [ span
    ( textStyle "1em" )
    [ text "Nombre de la escala:" ]
  , input
      ( squarefieldStyle "150px" ++
        [ style "margin-left" "30px"
        , placeholder "Cromática"
        , onInput CambiarNombreEscala
        ]
      )
      []
  ]


descargar : Html Msg
descargar =
  div []
    [ button
        ( buttonStyle "150px" ++
          [ style "margin" "0px 0px 50px 0px"
          , onClick Descargar
          ]
        )
        [ text "Descargar" ]
    ]

view : Model -> Browser.Document Msg
view model =
  (Browser.Document "Escalas"
    [ div generalStyle
        [ navbar 4
        , titulo "FUNCIÓN A ESCALAS"
        , dragNdrop model
        , nombreEscala
        , entrada "400px" "0 1 2 3 4 5 6 7 8 9 10 11" (onInput Introducir)
        , descargar
        , informacion
        , br [] []
        , credits
        ]
    ]
  )
