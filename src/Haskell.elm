module Haskell exposing (function)
import Array exposing (Array)

function : Scale -> Array Int
function lista =
    best <| induce lista

type alias Scale = List Int

long : Int
long = 12

best : List Scale -> Scale
best scales =
         head
      <| List.map Tuple.first
      <| List.sortBy Tuple.first
      <| List.sortBy Tuple.second --[(f,1),(s,2),(d,2),(g,4)]
      <| List.map --[(s,2),(d,2),(f,1),(g,4)]
        (\s -> (s, norm s))
        scales --[s,d,f,g]

head : List (List a) -> List a
head a =
  case a of
    [] -> []
    x::xs -> x

headx : List Int -> Int
headx a =
  case a of
    [] -> 0
    x::xs -> x

norm : Scale -> Int
norm s
  = List.sum  -- La suma de las distancias
  <| List.map  -- entre la función y la escala cromática.
    (\(x, i) -> abs (x-i))
  <| List.map2 Tuple.pair s (List.range 0 11)
  -- = sqrt
  -- <| fromIntegral
  -- <| sum
  -- <| List.map
  --   (\(x, i) -> (x-i)*(x-i))
  -- <| zip s [0..]


-- Llama a la función recursiva de fixed points.
induce : Scale -> List Scale
induce scale =
  fxp (List.length scale) scale []


-- Obliga a que haya alguna fixed.
-- ¿Existe alguna solución que no tenga 1 fixed pero sí 0?
-- He puesto demasiados head fixed, supongamos que no.
fxp : Int -> Scale -> List Scale -> List Scale
fxp a b c =
  case (a,b,c) of
-- Si encontró solución, devuélvelo.
    (_, _, (x::xs)) ->
      (x::xs)
-- Si 0 fixed points, no hay solución.
    (0, _, _) ->
      []
-- Si quedan fixed points, llama a func y disminuye nfixed.
    (nfixed, scale, _) ->
      fxp (nfixed - 1) scale (func nfixed scale)

-- TODO imprmamaarlo de sitio existente
-- #26 de H-99
combinations : Int -> Scale -> List Scale
combinations a b =
  case (a,b) of
    (0, _) -> [[]]
    (_, []) -> []
    (n, (x::xs)) ->
      List.map (\s -> x::s)
      ((combinations (n-1) xs)
      ++ (combinations n xs))


dissonances : Scale -> List Scale
dissonances scale =
  let e = List.length scale
      q = e - (modBy long e)
  in
  combinations q scale


-- q es el tamaño del subconjunto de frecuencias menores.
-- c es la frecuencia menor.
-- long = qc + (e - q)(c + 1)

guard : Bool -> (a -> List a)
guard b =
  case b of
    True -> List.singleton
    False -> always []

bind : List a -> (a -> List a) -> List a
bind xs f = List.concatMap f xs

rmama : List a -> List a -> List a
rmama a1 a2 =  List.map2 (\a b -> a b) ((List.map << always) identity a1) a2

dropWhile : (a -> Bool) -> List a -> List a
dropWhile p b =
  case b of
    [] -> []
    (x::xs) ->
      if p x
        then dropWhile p xs
        else xs

iterate : Int -> (Scale -> Scale) -> Scale -> List Scale
iterate n f x =
  if n == 0 then [x] else
  x :: iterate (n-1) f (f x)


func : Int -> Scale -> List Scale
func nfixed scale
  = let e = List.length scale
        c = long // e
    in
      bind (combinations nfixed scale) (\fixed ->
      bind (dissonances scale) (\minFreq ->
        let
          h = headx fixed
          hFreq =
            if List.member h minFreq
              then c
              else c + 1
          ( (first, freqFirst) :: oldFreqs )
            = List.map
              (\ha ->
                if List.member ha minFreq
                  then (ha, c)
                  else (ha, c + 1)
               )
            <| List.take e
            <| dropWhile (\i -> i < h)
              (scale ++ scale)
        in
        bind (iterate hFreq (\a -> a ++ [h]) [h]) (\start ->
          let
            freqs  -- Ponemos el h al final con su frecuencia ya restada.
              = oldFreqs
              ++ if freqFirst /= List.length start
                  then [ (first, freqFirst - List.length start) ]
                  else []
            newfixed
              = List.take (nfixed - 1)
              <| dropWhile (\a -> a <= h)
               (fixed ++ fixed)
          in
      rmama
      ((guard
        (List.length fixed == 1
        || (headx (List.drop 1 fixed)) - (headx fixed) >= List.length start)) (oneCase newfixed freqs start))

      (\result ->
      rmama ((guard (List.length result == long)) result)
      (normalize result)
      )
      )))))

splitAt : Int -> Scale -> (Scale,Scale)
splitAt n xs
  =  (List.take n xs, List.drop n xs)

span : (a -> Bool) -> List a -> (List a, List a)
span a b =
  case (a,b) of
    (_, []) -> ([], [])
    (p,(x::xs)) ->
        if (p x) then
           let (ys,zs) = span p xs
           in (x::ys,zs)
        else ([],(x::xs))


-- Pongo el índice 0 otra vez al principio
-- y lo pongo en orden creciente.
normalize : Scale -> Scale
normalize a =
  case a of
    [] -> []
    (h::rest) ->
      let
        (hhafter, before)
          = splitAt (long - h) (h::rest)
        (hh, after)
          = span ((==) h) hhafter
      in
        List.map (\i -> if i >  h then i - long else i) before
      ++ hh
      ++ List.map (\i -> if i <= h then i + long else i) after



-- Dados los puntos fijos, las frecuencias y el resultado
oneCase : Scale -> List (Int, Int) -> Scale -> Scale
oneCase a b c =
  case (a, b, c) of
    (_, _, []) ->
      []  -- Si no hay start ha habido un problema. No puede ocurrir.
    (_, [], x) ->
      x   -- Si no quedan frecuencias, hemos acabado bien.
    ([], ((next, nextFreq)::freqs), start) ->
      -- Si no quedan fixed points, simplemente rellenar.
      oneCase
      []
      freqs <| start ++ List.repeat nextFreq next

-- Si quedan fixed points::
    ((f::fixed), ((next, nextFreq)::freqs), (s::start)) ->
      let
        currentIndex
          = s + List.length start + 1
        eraseFreq element freq
          = if freq == 1
              then []
              else [(element, freq-1)]
      in
    -- Si f es igual al siguiente índice, next tiene que ser ese índice porque f es fijo.
      if (f==currentIndex && f==next) then
        oneCase
          fixed  -- Ya hemos usado f; lo desechamos.
          ((eraseFreq next nextFreq) ++ freqs) <|
          (s::start) ++ [next]
      --Si no necesito fijar, entonces next no puede pasarse del siguiente fixed point f.
      else if (f/=currentIndex && f>=next) then
        oneCase
          (f::fixed)
          ((eraseFreq next nextFreq) ++ freqs) <|
          (s::start) ++ [next]
      else []
